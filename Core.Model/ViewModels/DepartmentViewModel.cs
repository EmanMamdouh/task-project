﻿namespace Core.Model.ViewModels
{
    public class DepartmentViewModel
    {
        public int DepartmentId { get; set; }
        public int Name { get; set; }
        public bool IsParent { get; set; }
    }
}
