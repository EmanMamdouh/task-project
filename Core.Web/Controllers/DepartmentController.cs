﻿using Core.Model;
using Core.Model.ViewModels;
using Core.Service;
using Core.Service.Services;
using Core.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;

namespace Core.Web.Controllers
{
    public class DepartmentController : BaseController
    {

        private IServiceWrapper _serviceWrapper;

        public DepartmentController(IOptions<MailSettings> _mailSettings, IServiceWrapper serviceWrapper, IWebHostEnvironment webHostEnvironment):base(_mailSettings,webHostEnvironment)
        {
            _serviceWrapper = serviceWrapper;
        }

      
        public IActionResult Index()
        {
            ViewBag.Departments = (List<SelectListItem>)_serviceWrapper.departmentServices.GetSelectDepartments();
            return View();
        }



        public IActionResult AddDepartment()
        {
            ViewBag.Departments = (List<SelectListItem>)_serviceWrapper.departmentServices.GetSelectDepartments();
            return View();
        }


        [HttpPost]
        public IActionResult AddDepartment(Department model)
        {
            if (ModelState.IsValid)
            {
                _serviceWrapper.departmentServices.CreateDepartment(model);
                _serviceWrapper.departmentServices.SaveDepartment();
                return RedirectToAction("AddDepartment");
            }
                return View();
        }

        public IActionResult GetDepartments(int id)
        {  
            var model = _serviceWrapper.departmentServices.GetDepartment(id);

            List<Department> ParentDepartments = GetParents(model).ToList();
            ParentDepartments.Reverse();

            ViewBag.department = model.Name;
            return PartialView("_GetDepartments",new DepartmentList { SubDepartments = GetChildern(id), model = model, ParentDepartments = ParentDepartments });
        }

        public List<Department> GetChildern(int id)
        {
          var model= _serviceWrapper.departmentServices.GetChildern(id);
            return model;
        }

        IEnumerable<Department> GetParents(Department model)
        {
            Department dParent = model.ParentDepartment;
            while (dParent != null)
            {
                yield return dParent;
                dParent = dParent.ParentDepartment;
            }
        }

  

        public JsonResult CheckExistingName(string Name, int DepartmentId)
        {
            var res = _serviceWrapper.departmentServices.CheckName(Name);
            if (res != null)
            {
                if (res.DepartmentId != DepartmentId)
                {
                    return Json(false);
                }
            }
            return Json(true);
        }


    }
}
