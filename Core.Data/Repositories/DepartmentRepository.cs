﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Data.Repositories
{
    public class DepartmentRepository : IRepository<Department>, IDepartmentRepository
    {
        private readonly TaskDbContext _db;
        public DepartmentRepository(TaskDbContext db)
        {
            _db = db;
        }

        public void Add(Department entity)
        {
            _db.Departments.Add(entity);
        }
        public void Update(Department entity)
        {
            _db.Departments.Update(entity);
        }
        public void Commit()
        {
            _db.SaveChanges();
        }

        public IQueryable<Department> List()
        {
            return _db.Departments;
        }


        public Department Find(int id)
        {
            return _db.Departments.Find(id);
        }

    }

    public interface IDepartmentRepository : IRepository<Department>
    {

    }


}
