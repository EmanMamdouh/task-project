﻿using Core.Model;

namespace Core.Web.ViewModels
{
    public class DepartmentList
    {
        public Department model { get; set; }
        public List<Department> SubDepartments { get; set; }
        public List<Department> ParentDepartments { get; set; }
    }
}
