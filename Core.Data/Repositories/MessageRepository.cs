﻿using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Data.Repositories
{
    public class MessageRepository : IRepository<Message>, IMessageRepository
    {
        private readonly TaskDbContext _db;
        public MessageRepository(TaskDbContext db)
        {
            _db = db;
        }

        public void Add(Message entity)
        {
            _db.Messages.Add(entity);
        }
        public void Update(Message entity)
        {
            _db.Messages.Update(entity);
        }
        public void Commit()
        {
            _db.SaveChanges();
        }

        public IQueryable<Message> List()
        {
            return _db.Messages;
        }


        public Message Find(int id)
        {
            return _db.Messages.Find(id);
        }

    }

    public interface IMessageRepository : IRepository<Message>
    {

    }
}
