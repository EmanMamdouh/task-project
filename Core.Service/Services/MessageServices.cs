﻿using Core.Data;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Service.Services
{

    public class MessageServices : IMessageServices
    {
        private readonly IRepositoryWrapper _repositoryWrapper;


        public MessageServices(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;

        }


        public void CreateMessage(Message Message)
        {
            _repositoryWrapper.messageRepository.Add(Message);
        }

        public void SaveMessage()
        {
            _repositoryWrapper.messageRepository.Commit();
        }

        public Message GetMessage(int id)
        {
            return _repositoryWrapper.messageRepository.Find(id);
        }
        public List<Message> GetMessages()
        {
            return _repositoryWrapper.messageRepository.List().ToList();
        }

        public void UpdateMessage(Message Message)
        {
             _repositoryWrapper.messageRepository.Update(Message);
        }
    }

    public interface IMessageServices
    {
      
        List<Message> GetMessages();
        Message GetMessage(int id);

        void CreateMessage(Message Message);
        void UpdateMessage(Message Message);

        void SaveMessage();
    }
}

