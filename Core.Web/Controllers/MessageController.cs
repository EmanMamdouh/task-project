﻿using Core.Model;
using Core.Model.ViewModels;
using Core.Service;
using Core.Web.ViewModels;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Core.Web.Controllers
{
    public class MessageController : BaseController
    {

        private IServiceWrapper _serviceWrapper;
        private IBackgroundJobClient _backgroundJobClient;
        public MessageController(IOptions<MailSettings> _mailSettings, IBackgroundJobClient backgroundJobClient, IServiceWrapper serviceWrapper, IWebHostEnvironment webHostEnvironment) : base(_mailSettings, webHostEnvironment)
        {
            _serviceWrapper = serviceWrapper;
            _backgroundJobClient = backgroundJobClient;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public IActionResult AddMessage(Message model)
        {
            if (ModelState.IsValid)
            {
                _serviceWrapper.messageServices.CreateMessage(model);
                _serviceWrapper.messageServices.SaveMessage();

                //ToEmail set static to test only
                MailRequest request = new MailRequest { Subject = "Task-Project", ToEmail = "emanshaker381@gmail.com", Body = model.Title };

                var selectedDate = DateTimeOffset.Parse(model.SendTime.ToString("yyyy-MM-dd hh:mm:ss"));
                BackgroundJob.Schedule(() => SendMessage(request, model), selectedDate);

                return Json("1");
            }
            return Json("-1");
        }


        public async Task SendMessage(MailRequest request, Message model)
        {
            try
            {
                await _serviceWrapper.MailService.SendEmailAsync(request);
                model.IsSend = true;
            }
            catch (Exception ex)
            {
                model.IsSend = false;
            }

            _serviceWrapper.messageServices.UpdateMessage(model);
            _serviceWrapper.messageServices.SaveMessage();
        }


        public IActionResult GetMessages()
        {
            return PartialView("_MessageList", _serviceWrapper.messageServices.GetMessages().OrderBy(x => x.SendTime).ToList());
        }
    }
}
