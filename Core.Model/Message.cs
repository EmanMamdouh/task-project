﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    public class Message
    {
        public int? MessageId { get; set; }

        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Required(ErrorMessage = " ")]
        public DateTime SendTime { get; set; }
        public bool? IsSend { get; set; }
        public DateTime CeationDate { get; set; } = DateTime.Now;
    }
}
