﻿using Core.Data.Repositories;
using Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc.Rendering;
using Core.Model.ViewModels;
using Core.Data;

namespace Core.Service.Services
{
    public class DepartmentServices : IDepartmentServices
    {
        private readonly IRepositoryWrapper _repositoryWrapper;


        public DepartmentServices(IRepositoryWrapper repositoryWrapper)
        {
           _repositoryWrapper = repositoryWrapper;
          
        }
        public List<SelectListItem> GetSelectDepartments()
        {
            return _repositoryWrapper.departmentRepository.List().Select(x => new SelectListItem
            {
                Text = x.Name ,
                Value = x.DepartmentId.ToString()
            }).ToList();
        }

        public void CreateDepartment(Department Department)
        {
            _repositoryWrapper.departmentRepository.Add(Department);
        }

        public void SaveDepartment()
        {
            _repositoryWrapper.departmentRepository.Commit();
        }

        public Department GetDepartment(int id)
        {
            return _repositoryWrapper.departmentRepository.Find(id);
        }
        public List<Department> GetDepartments()
        {
            return _repositoryWrapper.departmentRepository.List().ToList();
        }
        public List<Department> GetChildern(int ParentId)
        {
            return GetDepartments().Where(x => x.ParentId == ParentId).ToList();
        }
        public Department? CheckName(string name)
        {
            return GetDepartments().FirstOrDefault(x => x.Name.Equals(name.Trim()));
        }
      
    }

    public interface IDepartmentServices
    {
        List<SelectListItem> GetSelectDepartments();
        List<Department> GetDepartments();
        Department GetDepartment(int id);
        List<Department> GetChildern(int ParentId);

        void CreateDepartment(Department Department);
        public Department? CheckName(string name);
        void SaveDepartment();
    }
}
