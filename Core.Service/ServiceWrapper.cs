﻿using Core.Data;
using Core.Model.ViewModels;
using Core.Service.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Service
{
    public class ServiceWrapper : IServiceWrapper
    {

        private readonly IRepositoryWrapper _repoWrapper;

        private readonly IOptions<MailSettings> _mailSettings;
        private IHostingEnvironment _env;
        public ServiceWrapper(IHostingEnvironment env, IRepositoryWrapper repoWrapper, IOptions<MailSettings> mailSettings)
        {
            this._repoWrapper = repoWrapper;
            _env = env;
            _mailSettings = mailSettings;
        }


        private IDepartmentServices _department;
        private IMessageServices _message;
        private IMailService _mail;

        public IDepartmentServices departmentServices
        {
            get
            {
                if (_department == null)
                {
                    _department = new DepartmentServices(_repoWrapper);
                }
                return _department;
            }
        }


        public IMessageServices messageServices
        {
            get
            {
                if (_message == null)
                {
                    _message = new MessageServices(_repoWrapper);
                }
                return _message;
            }
        }

        public IMailService MailService
        {
            get
            {
                if (_mail == null)
                {
                    _mail = new MailService(_env, _mailSettings);
                }
                return _mail;
            }
        }

    }

    public interface IServiceWrapper
    {
        IDepartmentServices departmentServices { get; }
        IMessageServices messageServices { get; }
        IMailService MailService { get; }
    }
 }
