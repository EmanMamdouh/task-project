﻿using Core.Model.ViewModels;
using Core.Web.ViewModels;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System.Net;

namespace Core.Web.Controllers
{
    public class BaseController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly MailSettings _mailSettings;

        public BaseController(IOptions<MailSettings> mailSettings, IWebHostEnvironment webHostEnvironment)
        {
            _mailSettings = mailSettings.Value;
            _webHostEnvironment = webHostEnvironment;

        }

        [HttpPost]
        public IActionResult RemoveUpload(string id)
        {
            string FolderName = "Attachments";

            try
            {
                // Check if file exists with its full path
                var t = Path.Combine(FolderName, id);
                if (System.IO.File.Exists(Path.Combine(_webHostEnvironment.WebRootPath,FolderName, id)))
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    // If file found, delete it    
                    System.IO.File.Delete(Path.Combine(_webHostEnvironment.WebRootPath,FolderName, id));
               
                }
                else return Json("-2");
            }
            catch (IOException ex)
            {
                return Json("-1");
            }
            return Json("1");
        }

        [HttpPost]
        public ActionResult UploadFile()
        {
            try
            {
                var files = Request.Form.Files;
                IFormFile file = files.FirstOrDefault();
                if (file.Length > 0)
                {
                    string uniqueFileName;

                    string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "Attachments");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }
                    return Json(new
                    {
                        name = uniqueFileName,
                    });
                }
                return Json(new { message = "Sorry No File To Upload .." });
            }
            catch (Exception ex)
            {
                return Json(new { message = "Sorry Can't Upload Your File .." + ex.Message });
            }

        }



        public async Task<int> SendEmailAsync(MailRequest mailRequest)
        {



            try { 
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("basmaa",_mailSettings.Mail));
            mailMessage.To.Add(new MailboxAddress("eman",mailRequest.ToEmail));
            mailMessage.Subject = "SendMail_MailKit_WithDomain";
            mailMessage.Body = new TextPart(TextFormat.Plain)
            {
                Text = "Hello"
            };

            using (var smtpClient = new MailKit.Net.Smtp.SmtpClient())
            {
                smtpClient.Connect(_mailSettings.Host, 25, MailKit.Security.SecureSocketOptions.StartTls);
                    smtpClient.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                    smtpClient.Send(mailMessage);
                smtpClient.Disconnect(true);
            }
            return 1;
            }
            catch (Exception ex)
            {
                var t = ex.Message;
                return -1;

            }
            //var email = new MimeMessage();
            //email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            //email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
            //email.Subject = mailRequest.Subject;
            //var builder = new BodyBuilder();

            //builder.HtmlBody = mailRequest.Body;
            //email.Body = builder.ToMessageBody();
            //using var smtp = new SmtpClient();
            //try
            //{
            //    using (var client = new ImapClient())
            //    {
            //        smtp.Connect(_mailSettings.Host, 25, MailKit.Security.SecureSocketOptions.None);
            //        smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
            //        await smtp.SendAsync(email);
            //    }
            //    return 1;
            //}
            //catch (Exception ex)
            //{
            //    var t = ex.Message;
            //    return -1;

            //}

            //smtp.Disconnect(true);
        }
    }
}
