﻿using Core.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Data
{
    public class RepositoryWrapper : IRepositoryWrapper
    {

        private TaskDbContext _TaskDbContext;

        private IDepartmentRepository _department;
        private IMessageRepository _message;

        public RepositoryWrapper(TaskDbContext TaskDbContext)
        {
            _TaskDbContext = TaskDbContext;
        }
        public IDepartmentRepository departmentRepository
        {
            get
            {
                if (_department == null)
                {
                    _department = new DepartmentRepository(_TaskDbContext);
                }
                return _department;
            }
        }

        public IMessageRepository messageRepository
        {
            get
            {
                if (_message == null)
                {
                    _message = new MessageRepository(_TaskDbContext);
                }
                return _message;
            }
        }

    }


    public interface IRepositoryWrapper
    {
        IDepartmentRepository departmentRepository { get; }
        IMessageRepository messageRepository { get; }
    
    }

}
