﻿using Core.Model.ViewModels;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Service.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;
        private IHostingEnvironment _env;
        public MailService(IHostingEnvironment env, IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
            _env = env;
        }


        public async Task<int> SendEmailAsync(MailRequest mailRequest)
        {
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);
            email.To.Add(MailboxAddress.Parse(mailRequest.ToEmail));
            email.Subject = mailRequest.Subject;
            var builder = new BodyBuilder();

            builder.HtmlBody = mailRequest.Body;
            email.Body = builder.ToMessageBody();
            using var smtp = new MailKit.Net.Smtp.SmtpClient();
            try
            {
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);

                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                smtp.Disconnect(true);
                return 1;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                smtp.Disconnect(true);
                return -1;
            }

        }

    }
    public interface IMailService
    {
        Task<int> SendEmailAsync(MailRequest mailRequest);
       
    }
}
