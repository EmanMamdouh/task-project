﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Model
{
    public class Department
    {
        public Department()
        {
            ChildDepartments = new HashSet<Department>();
        }

        [Key]
        public int? DepartmentId { get; set; }

        [Required(ErrorMessage =" ")]
        [Remote("CheckExistingName", "Department", AdditionalFields = "DepartmentId", ErrorMessage = "Department already Exist")]
        public string Name { get; set; }
        public string? Logo { get; set; }


        [ForeignKey("ParentDepartment")]
        public int? ParentId { get; set; }

        public DateTime CreationDate { get; set; } = DateTime.Now;


        public virtual Department? ParentDepartment { get; set; }
        public virtual ICollection<Department> ChildDepartments { get; set; }
    }
}
